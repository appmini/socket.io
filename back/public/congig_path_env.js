const dotenv = require('dotenv');
const path = require('path');

// Spécifiez le chemin complet vers votre fichier .env
const envPath = path.resolve(__dirname, 'config', '.env');

// Chargez les variables d'environnement à partir du fichier .env
dotenv.config({path: envPath});
