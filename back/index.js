import dotenv from 'dotenv';
import express from 'express';
const app = express();
//
import http from 'http';
const server = http.createServer(app);
//
import {fileURLToPath} from 'url';
import path from 'path';
//
import {Server} from 'socket.io';
const io = new Server(server);

//
const __filename = fileURLToPath(import.meta.url);
console.log('🚀 ~ __filename:', __filename);
const __dirname = path.dirname(__filename);

console.log('DirName', __dirname); // Affiche le répertoire parent du module actuel

//route static
const publicDirectoryPath = path.join(__dirname, 'public');
console.log('🚀 ~ publicDirectoryPath:', publicDirectoryPath);

app.use(express.static(publicDirectoryPath));
//
app.get('/', (req, res) => {
	const filePath = path.join(__dirname, '..', 'front', 'index.html');
	res.sendFile(filePath);
});

app.get('/chat', (req, res) => {
	const filePath = path.join(__dirname, '..', 'front', 'chat.html');
	res.sendFile(filePath);
});

io.on('connection', (socket) => {
	// console.log('🚀 ~ io.on ~ socket:', socket);
	const time = new Date().toLocaleTimeString();
	const tempUser = socket.id.substring(0, 5);
	console.log(`Un utilisateur '${tempUser}' s'est connecté à: ${time}`);
	socket.on('disconnect', () => {
		console.log('user disconnected');
	});

	socket.on('chat message', (msg) => {
		console.log('message: ' + msg);
	});
});

server.listen(3500, () => {
	console.log('listening on *:3500');
});
